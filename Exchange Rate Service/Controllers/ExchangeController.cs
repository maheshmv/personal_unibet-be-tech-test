﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Exchange_Rate_Service.Model;
using System.Web.Configuration;

namespace Exchange_Rate_Service.Controllers
{
    public class ExchangeController : ApiController
    {
        private string baseUrl;
        private string Url;
        public ExchangeController()
        {
            string key = WebConfigurationManager.AppSettings["ApiKey"];
            baseUrl = WebConfigurationManager.AppSettings["BaseURL"];
            Url = String.Format("{0}?access_key={1}&symbols=", baseUrl, key);
        }
        
        [Route("api/{exchange}/{baseCurrency}/{targetCurrency}")]
        public async Task<HttpResponseMessage> Get(string baseCurrency, string targetCurrency)
        {
            if ((baseCurrency = CurrencyModel.ValidateParameter(baseCurrency)) == null)
            {
                return this.Request.CreateResponse(HttpStatusCode.NotFound, new { message = "[Error: Not Found] Please provide the valid baseCurrency parameter" });
            }
            else if ((targetCurrency = CurrencyModel.ValidateParameter(targetCurrency)) == null)
            {
                return this.Request.CreateResponse(HttpStatusCode.NotFound, new { message = "[Error: Not Found] Please provide the valid targetCurrency parameter" });
            }

            bool validCurrency = CurrencyModel.ValidateCurrency(baseCurrency, targetCurrency);
            if(validCurrency == false)
            {
                return this.Request.CreateResponse(HttpStatusCode.NotFound, new { message = "[Error: Not Found] The currency is either not valid or not supported" });
            }

            // Avoid the matching pairs of base target frequencies i.e. where baseCurrency = targetCurrency
            if (baseCurrency.Equals(targetCurrency))
            {
                
                JsonResponseModel obj = new JsonResponseModel
                {
                    baseCurrency = baseCurrency,
                    targetCurrency = targetCurrency,
                    exchangeRate = 1,
                    timestamp = GetTimeStamp()  // Get current timestamp
                };
                return this.Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            string BaseUrl = Url + baseCurrency;
            string TargetUrl = Url + targetCurrency;
            decimal rate = 0;
            using (var client = new HttpClient())
            {
                var Basecontent = await client.GetStringAsync(BaseUrl);
                var Targetcontent = await client.GetStringAsync(TargetUrl);
                CurrencyModel basemodel = JsonConvert.DeserializeObject<CurrencyModel>(Basecontent);
                CurrencyModel targetmodel = JsonConvert.DeserializeObject<CurrencyModel>(Targetcontent);
                if (basemodel != null && targetmodel != null &&
                     basemodel.success == true && targetmodel.success == true)
                {
                    // Avoid zero rate or divide by zero case. Will handle the issues from fixer.io
                    if (targetmodel.rates[targetCurrency].Equals(0)|| basemodel.rates[baseCurrency].Equals(0))
                    { 
                        this.Request.CreateResponse(HttpStatusCode.InternalServerError, new { message = "Oops... Something went wrong. Please try again after sometime" });
                    }
                    rate = targetmodel.rates[targetCurrency] / basemodel.rates[baseCurrency];
                }
                else
                {
                    return this.Request.CreateResponse(HttpStatusCode.NotFound, new { message="[Error: Not Found] The currency is either not valid or not supported"});
                }

                string ts = GetTimeStamp(targetmodel.timeStamp);
                JsonResponseModel obj = new JsonResponseModel
                {
                    baseCurrency = baseCurrency,
                    targetCurrency = targetCurrency,
                    exchangeRate = decimal.Round(rate,5),
                    timestamp = ts
                };
                return this.Request.CreateResponse(HttpStatusCode.OK,obj);
            }
        }


        [Route("api/{exchange}/")]
        public async Task<HttpResponseMessage> Get()
        {
            string EmptyStr = String.Empty ;
            return await GetTarget(EmptyStr);
        }

        [Route("api/{exchange}/{baseCurrency}")]
        public async Task<HttpResponseMessage> GetBase(string baseCurrency)
        {
            return await GetTarget(baseCurrency);
        }

        public  Task<HttpResponseMessage> GetTarget(string targetCurrency)
        {
            return Task.FromResult( this.Request.CreateResponse(HttpStatusCode.NotFound, 
                new
                {
                    message = String.Format("[Error: Insufficient Parameter] The parameters baseCurrency and targetCurrency must be supplied"
                    + Environment.NewLine 
                    + " For example: /api/exchange?basecurrency=XYZ&targetcurrency=YZX")
                }));
        }

        // Get Timestamp for either current time or the timestamp returned from fixer.io
        private string GetTimeStamp(string datetime = null) 
        {
            DateTime startDate = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            if(datetime == null)
            {
                TimeSpan t = DateTime.UtcNow - startDate;
                datetime = t.TotalSeconds.ToString();
            }
            DateTime time = startDate.AddSeconds(Convert.ToDouble(datetime));
            return String.Format("{0:s}", time);
        }
    }

    // Model of object to be returned as Json back to the client.
    public class JsonResponseModel
    {
        public string baseCurrency { get; set; }
        public string targetCurrency { get; set; }
        public decimal exchangeRate { get; set; }
        public string timestamp { get; set; }
    }
}
