﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exchange_Rate_Service.Model
{
    public class CurrencyModel
    {
        public bool success;
        public string timeStamp;
        public string Base;
        public DateTime date;
        public Dictionary<string, decimal> rates;

        private static List<string> SupportedCurrency = new List<string> { "AUD", "SEK","USD","GBP","EUR"};
        public static bool ValidateCurrency(string baseCurr, string targetCurr)
        {
            bool baseExist = SupportedCurrency.Contains(baseCurr);
            bool targetExist = SupportedCurrency.Contains(targetCurr);
            return baseExist && targetExist;
        }
        public static string ValidateParameter(string curr)
        {
            if (curr == null) return null;
            else return curr.Trim().ToUpper();
        }
    }
}
